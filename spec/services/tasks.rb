require_relative "base_api"

class Tasks < BaseApi
    def list_task(token)

        return self.class.get("/tasks",
            headers: {
                "Content-Type": "application/json",
                "Accept": 'application/vnd.tasksmanager.v2',
                "Authorization": "#{token}"
            })
    end
end